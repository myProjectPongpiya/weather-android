package com.krungsri.apptestutils.data

import com.krungsri.appfoundation.http.ApiException
import com.krungsri.appfoundation.http.ApiHeader

object ApiExceptionMocks {

    /**
     * Returns an [ApiException] with the provided error [code].
     */
    fun withCode(code: String): ApiException {
        val error = ApiHeader()
        return ApiException(error)
    }
}
