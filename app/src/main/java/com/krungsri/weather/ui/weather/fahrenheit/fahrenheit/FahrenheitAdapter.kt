package com.krungsri.weather.ui.weather.fahrenheit.fahrenheit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.weather.R

class FahrenheitAdapter : RecyclerView.Adapter<FahrenheitListViewHolder>() {

    private var items = mutableListOf<FahrenheitListItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FahrenheitListViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_fahrenheit_list, parent, false)
        return FahrenheitListViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: FahrenheitListViewHolder, position: Int) {
        holder.bindData(items[position])
    }

    /**
     * Sets [items] in this [RecyclerView].
     */
    fun setItems(items: List<FahrenheitListItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}