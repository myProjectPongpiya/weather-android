package com.krungsri.weather.ui.weather.fahrenheit.fahrenheit

data class FahrenheitListItem(
    val image: String,
    val date: String,
    val main: String,
    val temp: Double
)