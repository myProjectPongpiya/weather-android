package com.krungsri.weather.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.navigation.Navigation.findNavController
import com.krungsri.appfoundation.base.BaseActivity
import com.krungsri.weather.R
import com.krungsri.weather.databinding.ActivityRootBinding

class RootActivity : BaseActivity() {

    private val binding: ActivityRootBinding by lazy { ActivityRootBinding.inflate(layoutInflater) }

    private fun navController() = findNavController(this, R.id.nav_host_fragment)

    override fun onResume() {
        super.onResume()
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_SECURE,
//            WindowManager.LayoutParams.FLAG_SECURE
//        )
        binding.navHostFragment.isVisible = true
        binding.ivAppLogo.isGone = true
    }

    override fun onPause() {
        super.onPause()
//        window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//        )
        binding.ivAppLogo.isVisible = true
        binding.navHostFragment.isGone = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupStatusBar()
    }

    private fun setupStatusBar() {
        window.apply {
            statusBarColor = getColor(android.R.color.transparent)
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    override fun restartSession() {
        navController().navigate(navController().graph.startDestination)
    }
}