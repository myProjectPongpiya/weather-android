package com.krungsri.weather.ui.weather.fahrenheit.fahrenheit

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.appfoundation.extensions.setImage
import com.krungsri.appfoundation.graphics.DrImage
import com.krungsri.appfoundation.graphics.ImageTransformations
import com.krungsri.appfoundation.text.DateFormat
import com.krungsri.weather.R
import kotlinx.android.synthetic.main.item_celsius_list.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class FahrenheitListViewHolder(
    view: View,
) : RecyclerView.ViewHolder(view) {

    fun bindData(item: FahrenheitListItem) {
        itemView.ivTemp.setImage(onSetTempImage(item.image))
        itemView.tvTime.text = setDateFormat(item.date)
        itemView.tvDetail.text = setTemp(item.main, item.temp)
    }

    @SuppressLint("SimpleDateFormat")
    private fun setTemp(main: String?, temp: Double): String {
        val kelvinToFahrenheit = (((temp - 273) + 32) * 9) / 5
        return "$main, ${setDecimalFormat(kelvinToFahrenheit)}$TYPE_FAHRENHEIT"
    }

    @SuppressLint("SimpleDateFormat")
    private fun setDateFormat(createdDate: String?): String {
        val inputFormat = SimpleDateFormat(DateFormat.DR_FULL_DATE_TIME_SECONDS_SLASH.format, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(DateFormat.DR_DATETIME.format, Locale.ENGLISH)
        val date: Date = inputFormat.parse(createdDate)
        return outputFormat.format(date)
    }

    private fun onSetTempImage(image: String): DrImage {
        return if (image.isEmpty()) {
             DrImage.ResId(
                resId = R.drawable.ic_avatar_profile,
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        } else {
            DrImage.Url(
                url = "$PATH_IMAGE$image$TYPE_PNG",
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        }
    }

    private fun setDecimalFormat(number: Double): String {
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.FLOOR
        df.isDecimalSeparatorAlwaysShown = true
        return df.format(number).toString()
    }

    companion object {
        const val PATH_IMAGE = "https://openweathermap.org/img/w/"
        const val KELVIN = 273
        const val TYPE_PNG = ".png"
        const val TYPE_FAHRENHEIT = "°F"
    }
}