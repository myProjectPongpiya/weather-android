package com.krungsri.weather.ui.weather.celsius

import android.os.Bundle
import android.view.View
import com.krungsri.appdata.repository.response.ForecastResponse
import com.krungsri.appdata.repository.response.WeatherResponse
import com.krungsri.appfoundation.base.ViewBindingFragment
import com.krungsri.appfoundation.extensions.Utils
import com.krungsri.appfoundation.extensions.bindViewModel
import com.krungsri.appfoundation.lib.lifecycle.bindImage
import com.krungsri.appfoundation.lib.lifecycle.bindText
import com.krungsri.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.krungsri.weather.R
import com.krungsri.weather.databinding.ScreenWeatherCelsiusBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class CelsiusFragment :
    ViewBindingFragment<ScreenWeatherCelsiusBinding>(R.layout.screen_weather_celsius) {

    private val vm by bindViewModel { getViewModel(clazz = CelsiusViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenWeatherCelsiusBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(vm) {
            observerInitialize()
            withViewLifecycleOwner {
                layout.tvCity.bindText(this, city)
                layout.tvLon.bindText(this, longitude)
                layout.tvLat.bindText(this, latitudes)
                layout.ivWeather.bindImage(this, imageTemp)
                layout.tvTemp.bindText(this, temp)
                layout.tvTempMax.bindText(this, tempMax)
                layout.tvTempMin.bindText(this, tempMin)
                forecastList.observe {
                    layout.wgtCelsiusList.setItems(it)
                }
            }
        }
    }

    private fun observerInitialize() {
        with(vm) {
            withViewLifecycleOwner {
                parentFragmentManager.setFragmentResultListener(
                    OBSERVER_WEATHER_CELSIUS_SUCCESS,
                    this,
                ) { _, bundle ->
                    val model = Utils.gsonParser?.fromJson(
                        bundle.getString(KEY_MODEL),
                        WeatherResponse::class.java
                    )
                    weatherResponse.value = model
                }
                parentFragmentManager.setFragmentResultListener(
                    OBSERVER_FORECAST_CELSIUS_SUCCESS,
                    this,
                ) { _, bundle ->
                    val model = Utils.gsonParser?.fromJson(
                        bundle.getString(KEY_MODEL_FORECAST),
                        ForecastResponse::class.java
                    )
                    if (model != null) {
                        forecastResponse.value = listOf(model)
                        forecastDateResponse.value = model.list
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance() = CelsiusFragment()
        const val OBSERVER_WEATHER_CELSIUS_SUCCESS = "observerWeatherCelsiusSuccess"
        const val OBSERVER_FORECAST_CELSIUS_SUCCESS = "observerForecastCelsiusSuccess"
        const val KEY_MODEL = "model"
        const val KEY_MODEL_FORECAST = "model_forecast"
    }
}