package com.krungsri.weather.ui.weather.celsius.celsiusList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.weather.R

class CelsiusListAdapter : RecyclerView.Adapter<CelsiusListViewHolder>() {

    private var items = mutableListOf<CelsiusListItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CelsiusListViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_celsius_list, parent, false)
        return CelsiusListViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CelsiusListViewHolder, position: Int) {
        holder.bindData(items[position])
    }

    /**
     * Sets [items] in this [RecyclerView].
     */
    fun setItems(items: List<CelsiusListItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}