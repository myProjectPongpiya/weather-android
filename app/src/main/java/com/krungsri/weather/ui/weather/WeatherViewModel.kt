package com.krungsri.weather.ui.weather

import androidx.lifecycle.MutableLiveData
import com.krungsri.appdata.repository.response.ForecastResponse
import com.krungsri.appdata.repository.response.WeatherResponse
import com.krungsri.appdomain.usecase.GetForecastUseCase
import com.krungsri.appdomain.usecase.GetWeatherUseCase
import com.krungsri.appfoundation.base.BaseViewModel
import com.krungsri.appfoundation.base.ScreenFragment
import com.krungsri.appfoundation.extensions.doToggleLoadingStateOf
import com.krungsri.appfoundation.extensions.launchWith
import com.krungsri.appfoundation.flatlist.DrFlatListItem
import com.krungsri.appfoundation.http.ApiException
import com.krungsri.appfoundation.model.MutableLiveEvent
import com.krungsri.weather.ui.weather.celsius.CelsiusFragment
import com.krungsri.weather.ui.weather.fahrenheit.FahrenheitFragment
import kotlinx.coroutines.flow.onEach

class WeatherViewModel(
    private val getWeatherUseCase: GetWeatherUseCase,
    private val getForecastUseCase: GetForecastUseCase
) : BaseViewModel() {

    val viewPagerScreens = MutableLiveData<List<ScreenFragment>>()
    val viewPagerScreensTitle = MutableLiveData<List<String>>()

    val weatherResponse = MutableLiveData<WeatherResponse>()
    val weatherResponseSuccess = MutableLiveEvent<Boolean>()

    val forecastResponse = MutableLiveData<ForecastResponse>()
    val forecastResponseSuccess = MutableLiveEvent<Boolean>()

    val headingName = MutableLiveData("City in England")
    val listCity = MutableLiveData<List<DrFlatListItem.Item>>()

    fun initialize() {
        initScreens()
        initScreensTitle()
        mockListCity()
        onGetWeatherUseCase(FIRST_INITIAL_CITY)
        onGetForecastUseCase(FIRST_INITIAL_CITY)
    }

    private fun initScreens() {
        val screens: List<ScreenFragment> =
            listOf(
                CelsiusFragment.newInstance(),
                FahrenheitFragment.newInstance()
            )
        viewPagerScreens.value = screens
    }

    private fun initScreensTitle() {
        val screens: List<String> =
            listOf(
                CELSIUS,
                FAHRENHEIT
            )
        viewPagerScreensTitle.value = screens
    }

    fun onGetWeatherUseCase(selectedCity: String) {
        getWeatherUseCase.build(selectedCity)
            .doToggleLoadingStateOf(this)
            .onEach { onGetWeatherSuccess(it) }
            .launchWith(this, onError = { onGetWeatherError(it) })
    }

    private fun onGetWeatherSuccess(response: WeatherResponse) {
        weatherResponse.value = response
        weatherResponseSuccess.call()
    }

    private fun onGetWeatherError(throwable: Throwable) {
        val apiException = throwable as? ApiException
        showPopup(
            title = apiException?.title
        )
    }

    fun onGetForecastUseCase(selectedCity: String) {
        getForecastUseCase.build(selectedCity)
            .doToggleLoadingStateOf(this)
            .onEach { onGetForecastSuccess(it) }
            .launchWith(this, onError = { onGetForecastError(it) })
    }

    private fun onGetForecastSuccess(response: ForecastResponse) {
        forecastResponse.value = response
        forecastResponseSuccess.call()
    }

    private fun onGetForecastError(throwable: Throwable) {
        val apiException = throwable as? ApiException
        showPopup(
            title = apiException?.title
        )
    }

    private fun mockListCity() {
        listCity.value =
            listOf(
                DrFlatListItem.Item(id = "0", title = "London"),
                DrFlatListItem.Item(id = "1", title = "San Francisco")
            )
    }

    companion object {
        const val CELSIUS = "Celsius"
        const val FAHRENHEIT = "Fahrenheit"
        const val FIRST_INITIAL_CITY = "bangkok"
    }
}