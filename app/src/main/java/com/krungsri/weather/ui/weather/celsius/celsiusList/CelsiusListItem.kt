package com.krungsri.weather.ui.weather.celsius.celsiusList

data class CelsiusListItem(
    val image: String,
    val date: String,
    val main: String,
    val temp: Double
)