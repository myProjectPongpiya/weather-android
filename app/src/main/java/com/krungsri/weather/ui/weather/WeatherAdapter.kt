package com.krungsri.weather.ui.weather

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.krungsri.appfoundation.base.ScreenFragment

class WeatherAdapter(
    fragment: Fragment,
    private val screens: List<ScreenFragment>
) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return screens.size
    }

    override fun createFragment(position: Int): Fragment {
        return screens[position]
    }
}