package com.krungsri.weather.ui.weather.fahrenheit.fahrenheit

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import com.krungsri.appfoundation.list.DrBaseInfinityScrollList

class FahrenheitList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
) : DrBaseInfinityScrollList(context, attrs, defStyle) {

    private val adapter = FahrenheitAdapter()

    init {

        super.initRecyclerView(
            adapter = adapter,
            layout = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false),
            decor = null
        )
    }

    /**
     * Sets the [items] to be displayed.
     */
    fun setItems(items: List<FahrenheitListItem>?) {
        adapter.setItems(items ?: emptyList())
    }
}