package com.krungsri.weather.ui.weather

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.appfoundation.base.ViewBindingFragment
import com.krungsri.appfoundation.extensions.Utils
import com.krungsri.appfoundation.extensions.bindViewModel
import com.krungsri.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.krungsri.weather.R
import com.krungsri.weather.databinding.ScreenWeatherBinding
import com.krungsri.weather.ui.weather.celsius.CelsiusFragment.Companion.OBSERVER_FORECAST_CELSIUS_SUCCESS
import com.krungsri.weather.ui.weather.celsius.CelsiusFragment.Companion.OBSERVER_WEATHER_CELSIUS_SUCCESS
import com.krungsri.weather.ui.weather.fahrenheit.FahrenheitFragment.Companion.OBSERVER_FORECAST_FAHRENHEIT_SUCCESS
import com.krungsri.weather.ui.weather.fahrenheit.FahrenheitFragment.Companion.OBSERVER_WEATHER_FAHRENHEIT_SUCCESS
import org.koin.androidx.viewmodel.ext.android.getViewModel

class WeatherFragment :
    ViewBindingFragment<ScreenWeatherBinding>(R.layout.screen_weather) {

    private val vm by bindViewModel { getViewModel(clazz = WeatherViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenWeatherBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(vm) {
            initialize()
            setUpToolBar()
            initViewPager()
            observerViewPagerData()
            observerWeatherSuccess()
            onSearchCityField()
        }
    }

    private fun initViewPager() {
        layout.viewPager.apply {
            isSaveEnabled = true
            isUserInputEnabled = true
            offscreenPageLimit = 1
        }
    }

    private fun observerViewPagerData() {
        with(vm) {
            withViewLifecycleOwner {
                viewPagerScreens.observe { screens ->
                    viewPagerScreensTitle.observe { _ ->
                        layout.viewPager.adapter = WeatherAdapter(this@WeatherFragment, screens)
                        TabLayoutMediator(layout.tabLayout, layout.viewPager) { _, _ -> }.attach()
                    }
                }
            }
        }
    }

    private fun observerWeatherSuccess() {
        with(vm) {
            withViewLifecycleOwner {
                weatherResponseSuccess.observe {
                    val bundle = Bundle()
                    bundle.putString(KEY_MODEL, Utils.gsonParser?.toJson(weatherResponse.value))
                    parentFragmentManager.setFragmentResult(OBSERVER_WEATHER_CELSIUS_SUCCESS, bundle)
                    parentFragmentManager.setFragmentResult(OBSERVER_WEATHER_FAHRENHEIT_SUCCESS, bundle)

                    parentFragmentManager.setFragmentResultListener(
                        OBSERVER_WEATHER_CELSIUS_SUCCESS, this
                    ) { requestKey, result ->
                        childFragmentManager.setFragmentResult(requestKey, result)
                    }
                    parentFragmentManager.setFragmentResultListener(
                        OBSERVER_WEATHER_FAHRENHEIT_SUCCESS, this
                    ) { requestKey, result ->
                        childFragmentManager.setFragmentResult(requestKey, result)
                    }
                }
                forecastResponseSuccess.observe {
                    val bundle = Bundle()
                    bundle.putString(KEY_MODEL_FORECAST, Utils.gsonParser?.toJson(forecastResponse.value))
                    parentFragmentManager.setFragmentResult(OBSERVER_FORECAST_CELSIUS_SUCCESS, bundle)
                    parentFragmentManager.setFragmentResult(OBSERVER_FORECAST_FAHRENHEIT_SUCCESS, bundle)

                    parentFragmentManager.setFragmentResultListener(
                        OBSERVER_FORECAST_CELSIUS_SUCCESS, this
                    ) { requestKey, result ->
                        childFragmentManager.setFragmentResult(requestKey, result)
                    }
                    parentFragmentManager.setFragmentResultListener(
                        OBSERVER_FORECAST_FAHRENHEIT_SUCCESS, this
                    ) { requestKey, result ->
                        childFragmentManager.setFragmentResult(requestKey, result)
                    }
                }
            }
        }
    }

    private fun onSearchCityField() {
        with(vm) {
            layout.tiCity.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    onGetWeatherUseCase(query)
                    onGetForecastUseCase(query)
                    return true
                }
                override fun onQueryTextChange(text: String?): Boolean {
                    if (text.isNullOrEmpty()) {
                        onGetWeatherUseCase(FIRST_INITIAL_CITY)
                        onGetForecastUseCase(FIRST_INITIAL_CITY)
                    }
                    return true
                }
            })
        }
    }

    private fun setUpToolBar() {
        layout.toolbar.hideLeft()
        layout.toolbar.setTitle(getString(R.string.tools_bar_name))
    }

    companion object {
        const val KEY_MODEL = "model"
        const val KEY_MODEL_FORECAST = "model_forecast"
        const val FIRST_INITIAL_CITY = "bangkok"
    }
}