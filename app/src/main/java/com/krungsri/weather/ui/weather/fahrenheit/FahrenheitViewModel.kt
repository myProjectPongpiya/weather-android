package com.krungsri.weather.ui.weather.fahrenheit

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.krungsri.appdata.repository.response.ForecastResponse
import com.krungsri.appdata.repository.response.WeatherResponse
import com.krungsri.appfoundation.base.BaseViewModel
import com.krungsri.appfoundation.graphics.DrImage
import com.krungsri.appfoundation.graphics.ImageTransformations
import com.krungsri.weather.R
import com.krungsri.weather.ui.weather.fahrenheit.fahrenheit.FahrenheitListItem
import java.math.RoundingMode
import java.text.DecimalFormat

class FahrenheitViewModel(
    private val appContext: Application
) : BaseViewModel() {

    val weatherResponse = MutableLiveData<WeatherResponse>()

    val city = Transformations.map(weatherResponse) { it.name ?: EMPTY_DATA }
    val longitude = Transformations.map(weatherResponse) {
        appContext.getString(
            R.string.label_longitude,
            it.coord.lon ?: EMPTY_DATA
        )
    }
    val latitudes = Transformations.map(weatherResponse) {
        appContext.getString(
            R.string.label_longitude,
            it.coord.lat ?: EMPTY_DATA
        )
    }
    val imageTemp =
        Transformations.map(weatherResponse, this::onSetTempImage)
    val temp = Transformations.map(weatherResponse) { setTempKelvinToFahrenheit(it.main.temp) }
    val tempMax =
        Transformations.map(weatherResponse) { setTempKelvinToCelsiusMax(it.main.tempMax) }
    val tempMin =
        Transformations.map(weatherResponse) { setTempKelvinToCelsiusMin(it.main.tempMin) }

    val forecastResponse = MutableLiveData<List<ForecastResponse>>()
    val forecastDateResponse = MutableLiveData<List<ForecastResponse.ListData>>()
    val forecastList = Transformations.map(forecastDateResponse, this::setForecastList)

    private fun setTempKelvinToFahrenheit(tempKelvin: Double): String {
        val kelvinToFahrenheit = (((tempKelvin - 273) + 32) * 9) / 5
        val temp = setDecimalFormat(kelvinToFahrenheit)
        return appContext.getString(R.string.type_fahrenheit, temp)
    }

    private fun setTempKelvinToCelsiusMax(tempKelvin: Double): String {
        val kelvinToFahrenheit = (((tempKelvin - 273) + 32) * 9) / 5
        val temp = setDecimalFormat(kelvinToFahrenheit)
        return appContext.getString(R.string.label_max_fahrenheit, temp)
    }

    private fun setTempKelvinToCelsiusMin(tempKelvin: Double): String {
        val kelvinToFahrenheit = (((tempKelvin - 273) + 32) * 9) / 5
        val temp = setDecimalFormat(kelvinToFahrenheit)
        return appContext.getString(R.string.label_min_fahrenheit, temp)
    }

    private fun onSetTempImage(items: WeatherResponse): DrImage {
        return if (items.weather[0].icon.isNullOrEmpty()) {
            DrImage.ResId(
                resId = R.drawable.ic_avatar_profile,
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        } else {
            DrImage.Url(
                url = PATH_IMAGE + "${items.weather[0].icon}$TYPE_PNG",
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        }
    }

    private fun setForecastList(response: List<ForecastResponse.ListData>): List<FahrenheitListItem> {
        if (response.isNullOrEmpty()) return emptyList()
        return response.mapIndexed { _, responses ->
            FahrenheitListItem(
                image = responses.weather[0].icon ?: EMPTY_DATA,
                date = responses.dtTxt ?: EMPTY_DATA,
                main = responses.weather[0].main ?: EMPTY_DATA,
                temp = responses.main.temp
            )
        } ?: emptyList()
    }

    private fun setDecimalFormat(number: Double): String {
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.FLOOR
        df.isDecimalSeparatorAlwaysShown = true
        return df.format(number).toString()
    }

    companion object {
        const val EMPTY_DATA = ""
        const val PATH_IMAGE = "https://openweathermap.org/img/w/"
        const val TYPE_PNG = ".png"
    }
}