package com.krungsri.weather

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.krungsri.weather.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(appComponent)
        }

        bootstrapLogging()
    }

    private fun bootstrapLogging() {
        Timber.plant(Timber.DebugTree())
    }
}