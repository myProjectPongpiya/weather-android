package com.krungsri.weather.extentions

import android.content.Intent
import android.net.Uri

/**
 * Returns any deep links found in this [Intent].
 */
fun Intent.getDeepLink(): Uri? {
    // If a typical deep link was launched, the URI will be in the `data` field.
    if (data != null) {
        return data
    }
    return null
}
