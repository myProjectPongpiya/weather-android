package com.krungsri.weather.di.module

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.RetentionManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.krungsri.appfoundation.data.DeviceIdStore
import com.krungsri.appfoundation.gson.LocalDateDeserializer
import com.krungsri.appfoundation.gson.LocalDateSerializer
import com.krungsri.appfoundation.gson.ZonedDateTimeDeserializer
import com.krungsri.appfoundation.gson.ZonedDateTimeSerializer
import com.krungsri.appfoundation.http.interceptor.CommonHeaderInterceptor
import com.krungsri.appfoundation.http.interceptor.HeaderRequestInterceptor
import com.krungsri.appfoundation.lib.eventbus.EventBus
import com.krungsri.weather.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.threeten.bp.Clock
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

val coreModule = module {
    single { provideGson() }
    single { provideClock() }
    single { providesEventBus() }
    single { provideChuckerCollector(androidContext()) }

    factory { provideCommonHeaderInterceptor(get()) }
    factory { provideHeaderRequestInterceptor(get(), get(named("applicationId")), get()) }
    factory { provideConnectivityManager(androidContext()) }
}

fun provideClock(): Clock = Clock.system(ZoneId.systemDefault())

fun providesEventBus(): EventBus = EventBus()

fun provideGson(): Gson {
    return GsonBuilder()
        .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeSerializer())
        .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeDeserializer())
        .registerTypeAdapter(LocalDate::class.java, LocalDateSerializer())
        .registerTypeAdapter(LocalDate::class.java, LocalDateDeserializer())
        .create()
}

fun provideChuckerCollector(context: Context): ChuckerCollector {
    return ChuckerCollector(
        context = context,
        showNotification = true,
        retentionPeriod = RetentionManager.Period.ONE_HOUR
    )
}

fun provideCommonHeaderInterceptor(
    deviceIdStore: DeviceIdStore,
): CommonHeaderInterceptor {
    return CommonHeaderInterceptor(
        clientVersion = BuildConfig.VERSION_NAME,
        platform = "android/${Build.VERSION.RELEASE}",
        deviceIdStore = deviceIdStore
    )
}

fun provideHeaderRequestInterceptor(
    gson: Gson,
    appId: String,
    deviceIdStore: DeviceIdStore
): HeaderRequestInterceptor {
    return HeaderRequestInterceptor(
        gson = gson,
        appId = appId,
        clientVersion = BuildConfig.VERSION_NAME,
        platform = "android/${Build.VERSION.RELEASE}",
        deviceIdStore = deviceIdStore,
    )
}

fun provideConnectivityManager(context: Context): ConnectivityManager =
    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
