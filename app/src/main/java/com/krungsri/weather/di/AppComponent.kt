package com.krungsri.weather.di

import com.krungsri.weather.di.module.coreModule
import com.krungsri.weather.di.module.networkModule
import com.krungsri.weather.di.module.repositoryModule
import com.krungsri.weather.di.module.useCaseModule
import com.krungsri.weather.di.module.viewModelModule

val appComponent = listOf(
    coreModule, networkModule, repositoryModule, useCaseModule, viewModelModule
)