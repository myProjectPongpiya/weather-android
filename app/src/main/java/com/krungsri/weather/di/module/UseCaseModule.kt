package com.krungsri.weather.di.module

import com.krungsri.appdomain.usecase.GetForecastUseCase
import com.krungsri.appdomain.usecase.GetWeatherUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetWeatherUseCase(get()) }
    factory { GetForecastUseCase(get()) }
}