package com.krungsri.weather.di.module

import com.krungsri.weather.ui.weather.WeatherViewModel
import com.krungsri.weather.ui.weather.celsius.CelsiusViewModel
import com.krungsri.weather.ui.weather.fahrenheit.FahrenheitViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { WeatherViewModel(get(), get()) }
    viewModel { CelsiusViewModel(get()) }
    viewModel { FahrenheitViewModel(get()) }
}