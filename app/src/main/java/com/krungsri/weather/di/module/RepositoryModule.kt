package com.krungsri.weather.di.module

import com.krungsri.appdata.repository.api.WeatherApi
import org.koin.dsl.module
import retrofit2.Retrofit

val repositoryModule = module {
    single { provideWeatherApi(get()) }
}

fun provideWeatherApi(retrofit: Retrofit): WeatherApi {
    return retrofit.create(WeatherApi::class.java)
}