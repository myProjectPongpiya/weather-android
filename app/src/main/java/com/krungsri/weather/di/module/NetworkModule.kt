package com.krungsri.weather.di.module

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.krungsri.appfoundation.http.interceptor.CommonExceptionInterceptor
import com.krungsri.appfoundation.http.interceptor.ExceptionWrapperInterceptor
import com.krungsri.appfoundation.http.interceptor.HttpOverrideInterceptor
import com.krungsri.appfoundation.http.interceptor.NoConnectivityInterceptor
import com.krungsri.weather.BuildConfig
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    factory {
        provideHttpClient(
            androidContext(),
            get(),
            get(),
            get()
        )
    }

    factory { NoConnectivityInterceptor(get()) }
    factory { HttpOverrideInterceptor() }
    factory { CommonExceptionInterceptor(get()) }

    single { provideRetrofit(get(), get()) }
}

fun provideHttpClient(
    context: Context,
    checkForInternetConnectivity: NoConnectivityInterceptor,
    overrideHttpSettings: HttpOverrideInterceptor,
    checkForExceptions: CommonExceptionInterceptor,
): OkHttpClient {
    val builder = OkHttpClient.Builder()
        .addInterceptor(checkForInternetConnectivity)
        .addInterceptor(overrideHttpSettings)
        .addInterceptor(ExceptionWrapperInterceptor())
        .addInterceptor(checkForExceptions)
        .addInterceptor(chuckerInterceptor(context))
        .callTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .connectTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .readTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .writeTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)

    return builder.build()
}

fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_API_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

private fun chuckerInterceptor(
    context: Context
): ChuckerInterceptor {
    return ChuckerInterceptor.Builder(context)
        .collector(ChuckerCollector(context))
        .maxContentLength(CHUCKER_MAX_CONTENT_LENGTH)
        .redactHeaders(emptySet())
        .alwaysReadResponseBody(false)
        .build()
}

private const val CONNECTION_TIMEOUT_SECOND = 30.toLong()
private const val CHUCKER_MAX_CONTENT_LENGTH = 250000L
