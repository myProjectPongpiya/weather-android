package com.krungsri.appdata.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherResponse(
    @SerializedName("cod")
    val cod: Int,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("id")
    val id: Int,
    @SerializedName("timezone")
    val timezone: Int,
    @SerializedName("sys")
    val sys: Sys,
    @SerializedName("dt")
    val dt: Int,
    @SerializedName("clouds")
    val clouds: Clouds,
    @SerializedName("wind")
    val wind: Wind,
    @SerializedName("visibility")
    val visibility: Int,
    @SerializedName("main")
    val main: Main,
    @SerializedName("base")
    val base: String,
    @SerializedName("weather")
    val weather: List<Weather>,
    @SerializedName("coord")
    val coord: Coord,
) : Parcelable {

    @Parcelize
    data class Sys(
        @SerializedName("type")
        val type: Int,
        @SerializedName("id")
        val id: Int,
        @SerializedName("country")
        val country: String? = null,
        @SerializedName("sunrise")
        val sunrise: Int,
        @SerializedName("sunset")
        val sunset: Int,
    ): Parcelable

    @Parcelize
    data class Clouds(
        @SerializedName("all")
        val all: Int
    ) : Parcelable

    @Parcelize
    data class Wind(
        @SerializedName("speed")
        val speed: Double,
        @SerializedName("deg")
        val deg: Int
    ): Parcelable

    @Parcelize
    data class Main(
        @SerializedName("temp")
        val temp: Double,
        @SerializedName("feels_like")
        val feelsLike: Double,
        @SerializedName("temp_min")
        val tempMin: Double,
        @SerializedName("temp_max")
        val tempMax: Double,
        @SerializedName("pressure")
        val pressure: Double,
        @SerializedName("humidity")
        val humidity: Double
    ): Parcelable

    @Parcelize
    data class Weather(
        @SerializedName("id")
        val id: Int,
        @SerializedName("main")
        val main: String? = null,
        @SerializedName("description")
        val description: String? = null,
        @SerializedName("icon")
        val icon: String? = null,
    ): Parcelable

    @Parcelize
    data class Coord(
        @SerializedName("lon")
        val lon: Double,
        @SerializedName("lat")
        val lat: Double,
    ): Parcelable
}
