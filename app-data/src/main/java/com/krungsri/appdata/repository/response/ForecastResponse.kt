package com.krungsri.appdata.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ForecastResponse(
    @SerializedName("cod")
    val cod: Int,
    @SerializedName("message")
    val message: Double,
    @SerializedName("city")
    val city: City,
    @SerializedName("list")
    val list: List<ListData>,
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("timezone")
    val timezone: Int,
    @SerializedName("sunrise")
    val sunrise: Int,
    @SerializedName("sunset")
    val sunset: Int
) : Parcelable {

    @Parcelize
    data class ListData(
        @SerializedName("dt")
        val dt: Int,
        @SerializedName("main")
        val main: Main,
        @SerializedName("weather")
        val weather: List<Weather>,
        @SerializedName("clouds")
        val clouds: Clouds,
        @SerializedName("wind")
        val wind: Wind,
        @SerializedName("visibility")
        val visibility: Int,
        @SerializedName("pop")
        val pop: Double,
        @SerializedName("sys")
        val sys: Sys,
        @SerializedName("dt_txt")
        val dtTxt: String? = null
    ): Parcelable

    @Parcelize
    data class Main(
        @SerializedName("temp")
        val temp: Double,
        @SerializedName("feels_like")
        val feelsLike: Double,
        @SerializedName("temp_min")
        val tempMin: Double,
        @SerializedName("temp_max")
        val tempMax: Double,
        @SerializedName("pressure")
        val pressure: Int,
        @SerializedName("sea_level")
        val seaLevel: Int,
        @SerializedName("grnd_level")
        val grndLevel: Int,
        @SerializedName("humidity")
        val humidity: Int,
        @SerializedName("temp_kf")
        val tempKf: Double,
    ): Parcelable

    @Parcelize
    data class Weather(
        @SerializedName("id")
        val id: Int,
        @SerializedName("main")
        val main: String? = null,
        @SerializedName("description")
        val description: String? = null,
        @SerializedName("icon")
        val icon: String? = null
    ): Parcelable

    @Parcelize
    data class Clouds(
        @SerializedName("all")
        val all: Int
    ): Parcelable

    @Parcelize
    data class Wind(
        @SerializedName("speed")
        val speed: Double,
        @SerializedName("deg")
        val deg: Int,
        @SerializedName("gust")
        val gust: Double
    ): Parcelable

    @Parcelize
    data class Sys(
        @SerializedName("pod")
        val pod: String? = null
    ): Parcelable

    @Parcelize
    data class City(
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("coord")
        val coord: Coord
    ): Parcelable

    @Parcelize
    data class Coord(
        @SerializedName("lat")
        val lat: Double,
        @SerializedName("lon")
        val lon: Double,
    ): Parcelable
}
