package com.krungsri.appdata.repository.api

import com.krungsri.appdata.repository.response.ForecastResponse
import com.krungsri.appdata.repository.response.WeatherResponse
import retrofit2.http.POST
import retrofit2.http.Query

interface WeatherApi {

    @POST("weather?&appid=$API_KEY")
    suspend fun getWeather(@Query("q") username: String): WeatherResponse

    @POST("forecast?&appid=$API_KEY")
    suspend fun getForecast(@Query("q") username: String): ForecastResponse

    companion object {
        const val API_KEY = "8bd135fe02a486e68f2c0d2c3445ca66"
    }
}
