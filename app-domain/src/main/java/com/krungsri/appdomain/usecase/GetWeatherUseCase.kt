package com.krungsri.appdomain.usecase

import com.krungsri.appdata.repository.api.WeatherApi
import com.krungsri.appdata.repository.response.WeatherResponse
import com.krungsri.appfoundation.base.BaseUseCase
import com.krungsri.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetWeatherUseCase(
    private val repository: WeatherApi
) : BaseUseCase<String, WeatherResponse>() {
    override fun onBuild(params: String): Flow<WeatherResponse> {
        return flowSingle { repository.getWeather(params) }
    }
}