package com.krungsri.appdomain.usecase

import com.krungsri.appdata.repository.api.WeatherApi
import com.krungsri.appdata.repository.response.ForecastResponse
import com.krungsri.appfoundation.base.BaseUseCase
import com.krungsri.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetForecastUseCase(
    private val repository: WeatherApi
) : BaseUseCase<String, ForecastResponse>() {
    override fun onBuild(params: String): Flow<ForecastResponse> {
        return flowSingle { repository.getForecast(params) }
    }
}