package com.krungsri.appfoundation.model

import io.reactivex.disposables.CompositeDisposable

interface Disposer {
    val disposeBag: CompositeDisposable
}
