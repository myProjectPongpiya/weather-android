package com.krungsri.appfoundation.model

data class LoadingState(
    val isLoading: Boolean,
    val labels: List<String> = listOf()
)
