package com.krungsri.appfoundation.deeplink

/**
 * Represents an intent to navigate to a [uri] deep link. You may optionally
 * restrict the deep link to the current application using the [internalOnly] flag.
 */
data class DeepLinkEvent(
    val uri: String,
    val internalOnly: Boolean = false,
    val onNotSupported: (() -> Unit)? = null
)
