package com.krungsri.appfoundation.ui

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.navigation.NavDirections
import com.krungsri.appfoundation.http.ApiException
import com.krungsri.appfoundation.permissions.PermissionResult
import com.krungsri.appfoundation.text.CustomSpannable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

interface UIController : CoroutineScope {

    val disposeBag: MutableList<() -> Unit>

    /**
     * Sets the loading state of this UI controller.
     */
    fun setLoading(isLoading: Boolean, vararg labels: String)

    /**
     * Shows a toast [message].
     */
    fun showToast(message: String, toastType: Alert.ToastType = Alert.ToastType.SUCCESS)

    /**
     * Show a popup alert with [message].
     */
    fun showPopup(
        title: String? = null,
        message: String? = null,
        subMessage: CustomSpannable? = null,
        buttonLabel: String? = null,
        @DrawableRes heroImage: Int? = null,
        onDismiss: (() -> Unit)? = null,
    )

    /**
     * Show a popup alert with [message].
     */
    fun showPopupWithMultiButton(
        title: String? = null,
        message: String? = null,
        @DrawableRes heroImage: Int? = null,
        onDismiss: (() -> Unit)? = null,
        negativeButtonLabel: String? = null,
        negativeButtonListener: (() -> Unit)? = null,
        positiveButtonLabel: String? = null,
        positiveButtonListener: (() -> Unit)? = null,
        multiButtonType: Alert.MultiButtonType = Alert.MultiButtonType.HORIZONTAL
    )

    /**
     * Shows a native popup alert.
     */
    fun showNativePopup(
        title: String? = null,
        message: String? = null,
        negativeButtonLabel: String? = null,
        negativeButtonListener: (() -> Unit)? = null,
        positiveButtonLabel: String? = "OK",
        positiveButtonListener: (() -> Unit)? = null,
    )

    /**
     * Launches a [uri] deeplink.
     */
    fun launchDeepLink(uri: String, internalOnly: Boolean, onNotSupported: (() -> Unit)? = null)

    /**
     * Displays a no connection error state.
     */
    fun showNoConnectionError(onConnected: () -> Unit)

    /**
     * Displays dialog for contact to the medical council.
     */
    fun showPopupContactToMedicalCouncil(title: String)

    /**
     * Displays a restart app error state.
     */
    fun restartSession()

    /**
     * Checks for required [permissions] and receive the result of the permission
     * request in the [onDenied] or [onGranted] callbacks.
     *
     * If you need to handle the displaying of rationales, use
     * [requestPermissionsWithRationale] or [requestPermissionsFlow].
     */
    fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)? = null,
        onGranted: () -> Unit
    )

    /**
     * Checks for required [permissions] and receive the result of the permission
     * request in [onResult].
     */
    fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit
    )

    fun gotoPage(navDirections: NavDirections)

    fun goBack(
        @IdRes destinationId: Int? = null
    )

    fun onCallPhoneNumber(phoneNo: String)

    fun onCallEmail(email: String)

    fun onCallWebBrowser(link: String)

    fun onDeeplinkPreRoute(continuation: Continuation<Unit>)
}

/**
 * Shows the default alert for a [Throwable].
 */
fun UIController.showAlert(t: Throwable, onDismiss: (() -> Unit)? = null) {
    val apiException = t as? ApiException
    showPopup(
        title = apiException?.title ?: "API Error",
        message = apiException?.message ?: "API Error",
        onDismiss = onDismiss
    )
}

suspend fun UIController.awaitNativePopup(
    title: String? = null,
    message: String? = null,
    positiveButtonLabel: String? = "OK",
    negativeButtonLabel: String? = null
) = suspendCancellableCoroutine<Boolean> { continuation ->
    showNativePopup(
        title = title,
        message = message,
        positiveButtonLabel = positiveButtonLabel,
        negativeButtonListener = { continuation.resume(false) },
        negativeButtonLabel = negativeButtonLabel,
        positiveButtonListener = { continuation.resume(true) }
    )
}

/**
 * Checks for the required [permissions], returns `true` if the permission is
 * requested, or `false` otherwise.
 *
 * If you need to handle the displaying of rationales, use [requestPermissionsFlow].
 */
suspend fun UIController.awaitPermission(vararg permissions: String): Boolean =
    suspendCancellableCoroutine { continuation ->
        requestPermissions(
            *permissions,
            onDenied = { continuation.resume(false) },
            onGranted = { continuation.resume(true) }
        )
    }

/**
 * Checks for the required [permissions] and emits the [PermissionResult] through
 * this Flow. Multiple [PermissionResult] can be emitted if rationales are handled.
 *
 * Example of usage:
 *
 * ```
 * requestPermissionsFlow(Manifest.permissions.CAMERA)
 *   .transform { result ->
 *     when (result) {
 *       is PermissionResult.Granted -> emit(true)
 *       is PermissionResult.Denied -> emit(false)
 *       is PermissionResult.RationaleRequired -> {
 *         showToast("Permissions must be required to proceed")
 *
 *         // The result of this request will cause a new PermissionResult to
 *         // be emitted again.
 *         result.token.continuePermissionRequest()
 *       }
 *     }
 *   }
 * ```
 */
fun UIController.requestPermissionsFlow(vararg permissions: String): Flow<PermissionResult> =
    callbackFlow {
        requestPermissionsWithRationale(*permissions) { sendBlocking(it) }
        awaitClose()
    }
