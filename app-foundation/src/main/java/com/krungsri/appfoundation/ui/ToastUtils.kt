package com.krungsri.appfoundation.ui

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.krungsri.appfoundation.R
import kotlinx.android.synthetic.main.layout_toast.view.*

object ToastUtils {

    private fun showToast(
        context: Context,
        @DrawableRes background: Int,
        textColor: Int,
        image: Int,
        message: String
    ) {

        val toastView = LayoutInflater
            .from(context)
            .inflate(R.layout.layout_toast, null)

        toastView.findViewById<LinearLayout>(R.id.divToast).apply {
            ViewCompat.setBackground(this, ContextCompat.getDrawable(context, background))
            ivToastImage.setImageDrawable(ContextCompat.getDrawable(context, image))
            tvToastMessage.text = message
            tvToastMessage.setTextColor(ContextCompat.getColor(context, textColor))
        }

        val toast = Toast(context).apply {
            duration = Toast.LENGTH_SHORT
            view = toastView
            setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
        }

        toast.show()
    }
}
