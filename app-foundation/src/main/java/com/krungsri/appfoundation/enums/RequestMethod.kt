package com.krungsri.appfoundation.enums

enum class RequestMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}