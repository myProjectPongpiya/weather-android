package com.krungsri.appfoundation.graphics

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.util.Base64
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream

object BitmapUtils {

    /**
     * Creates a new borderd bitmap with [borderSize] and [borderColor].
     */
    fun addCircleBorder(bitmap: Bitmap, borderSize: Float, borderColor: Int): Bitmap {
        val borderOffset = (borderSize * 2).toInt()
        val halfWidth = bitmap.width / 2
        val halfHeight = bitmap.height / 2
        val circleRadius = Math.min(halfWidth, halfHeight).toFloat()
        val newBitmap = Bitmap.createBitmap(
            bitmap.width + borderOffset,
            bitmap.height + borderOffset,
            Bitmap.Config.ARGB_8888
        )

        // Center coordinates of the image
        val centerX = halfWidth + borderSize
        val centerY = halfHeight + borderSize

        val paint = Paint()
        val canvas = Canvas(newBitmap).apply {
            // Set transparent initial area
            drawARGB(0, 0, 0, 0)
        }

        // Draw the transparent initial area
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        canvas.drawCircle(centerX, centerY, circleRadius, paint)

        // Draw the image
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, borderSize, borderSize, paint)

        // Draw the border
        paint.xfermode = null
        paint.style = Paint.Style.STROKE
        paint.color = borderColor
        paint.strokeWidth = borderSize
        canvas.drawCircle(centerX, centerY, circleRadius, paint)

        return newBitmap
    }

    /**
     * Crop a [bitmap].
     */
    suspend fun crop(
        bitmap: Bitmap,
        x: Int,
        y: Int,
        width: Int,
        height: Int
    ): Bitmap = withContext(Dispatchers.IO) {
        return@withContext Bitmap.createBitmap(bitmap, x, y, width, height)
    }

    /**
     * Compress a [bitmap] as JPEG to a specific [maxSize] using [strategy].
     */
    suspend fun compressToSize(
        bitmap: Bitmap,
        maxSize: Int,
        strategy: CompressionStrategy
    ): ByteArray = withContext(Dispatchers.IO) {
        return@withContext when (strategy) {
            is CompressionStrategy.QUALITY -> bitmap.compressToSizeByQuality(
                maxSize = maxSize,
                qualityStep = strategy.qualityStep
            )
            is CompressionStrategy.DIMENSIONS -> bitmap.compressToSizeByDimensions(
                maxSize = maxSize,
                ratioStep = strategy.ratioStep,
                jpegQuality = strategy.quality
            )
        }
    }

    private suspend fun Bitmap.compressToSizeByQuality(
        maxSize: Int,
        qualityStep: Int,
        startAtQuality: Int = 100
    ): ByteArray = withContext(Dispatchers.IO) {
        if (qualityStep == 0) {
            throw IllegalArgumentException("qualityStep cannot be 0.")
        }

        if (startAtQuality < 0) {
            throw IllegalArgumentException("startAtQuality cannot be less than 0.")
        }

        val compressedByteArray = compressToJpeg(quality = startAtQuality)

        // If the compressed byte array is still larger than [maxSize], call this
        // function again recursively but with a lower quality.
        if (compressedByteArray.size > maxSize) {
            return@withContext compressToSizeByQuality(
                maxSize = maxSize,
                qualityStep = qualityStep,
                startAtQuality = startAtQuality - qualityStep
            )
        }

        return@withContext compressedByteArray
    }

    private suspend fun Bitmap.compressToSizeByDimensions(
        maxSize: Int,
        ratioStep: Float,
        jpegQuality: Int,
        startAtRatio: Float = 1f
    ): ByteArray = withContext(Dispatchers.IO) {
        if (ratioStep == 0f) {
            throw IllegalArgumentException("ratioStep cannot be 0.")
        }

        if (startAtRatio < 0) {
            throw IllegalArgumentException("startAtRatio cannot be less than 0.")
        }

        val resizedBitmap = if (startAtRatio != 1f) {
            resizeByRatio(startAtRatio)
        } else {
            this@compressToSizeByDimensions
        }

        val compressedByteArray = resizedBitmap.compressToJpeg(quality = jpegQuality)

        // If the compressed byte array is still larger than [maxSize], call this
        // function again recursively but with a lower ratio dimension.
        if (compressedByteArray.size > maxSize) {
            return@withContext compressToSizeByDimensions(
                maxSize = maxSize,
                ratioStep = ratioStep,
                jpegQuality = jpegQuality,
                startAtRatio = startAtRatio - ratioStep
            )
        }

        return@withContext compressedByteArray
    }

    private fun Bitmap.compressToJpeg(quality: Int): ByteArray {
        val output = ByteArrayOutputStream()
        this.compress(Bitmap.CompressFormat.JPEG, quality, output)
        return output.toByteArray().also { output.close() }
    }

    private fun Bitmap.resizeByRatio(ratio: Float): Bitmap {
        val newWidth = this.width * ratio
        val newHeight = this.height * ratio
        return Bitmap.createScaledBitmap(this, newWidth.toInt(), newHeight.toInt(), true)
    }

    fun Bitmap.getBase64String(
        compressFormat: Bitmap.CompressFormat,
        quality: Int
    ): String {
        val byteArrayOS = ByteArrayOutputStream()
        this.compress(compressFormat, quality, byteArrayOS)
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP)
    }

    sealed class CompressionStrategy {
        data class QUALITY(val qualityStep: Int = 5) : CompressionStrategy()
        data class DIMENSIONS(val ratioStep: Float = 0.1f, val quality: Int) : CompressionStrategy()
    }
}
