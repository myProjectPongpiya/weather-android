package com.krungsri.appfoundation.list

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

abstract class DrBaseInfinityScrollList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    private lateinit var scrollListener: DrListScrollListener

    protected fun initRecyclerView(adapter: Adapter<*>?, layout: LayoutManager, decor: ItemDecoration? = null) {
        super.setAdapter(adapter)
        super.setLayoutManager(layout)
        decor?.let { super.addItemDecoration(it) }
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        if (isInEditMode) {
            // Required by Android Studio preview
            return super.setAdapter(adapter)
        }

        throw IllegalStateException("Adapter cannot be configured for DrFlatList.")
    }

    override fun setLayoutManager(layout: LayoutManager?) {
        if (isInEditMode) {
            // Required by Android Studio preview
            super.setLayoutManager(layout)
        }

        throw IllegalStateException("Layout Manager cannot be configured for DrFlatList.")
    }

    override fun addItemDecoration(decor: ItemDecoration) {
        if (isInEditMode) {
            // Required by Android Studio preview
            super.addItemDecoration(decor)
        }

        throw IllegalStateException("Item Decoration cannot be configured for BNFlatList.")
    }

    fun setInfinityScrollListener(onLoadMore: (page: Int, size: Int) -> Unit) {
        scrollListener = object : DrListScrollListener() {
            override fun onLoadMore(page: Int, size: Int) {
                onLoadMore(page, size)
            }
        }
        addOnScrollListener(scrollListener)
    }

    fun notifyDataIsLoaded() {
        scrollListener.isLoading = false
    }

    fun setLastPageState(state: Boolean) {
        scrollListener.isLastPage = state
    }

    fun resetPagination() {
        if (::scrollListener.isInitialized) {
            scrollListener.currentPage = DrListScrollListener.INITIAL_PAGE
        }
    }

    fun setPagination(pageNumber: Int) {
        scrollListener.currentPage = pageNumber
    }
}
