package com.krungsri.appfoundation.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.annotation.IdRes
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import com.krungsri.appfoundation.permissions.PermissionResult
import com.krungsri.appfoundation.text.CustomSpannable
import com.krungsri.appfoundation.ui.Alert
import com.krungsri.appfoundation.ui.UIController
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

abstract class DrDialogFragment(

    /**
     * Disables the `Click Outside` state of the dialog.
     */
    private val disableClickOutside: Boolean = false,

    ) : DialogFragment(), UIController {

    val onViewCreatedListeners = mutableListOf<(View, Bundle?) -> Unit>()

    override fun onStart() {
        super.onStart()
        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.window?.setLayout(width, height)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setOnShowListener {
            if (disableClickOutside) {
                dialog.setCancelable(!disableClickOutside)
                dialog.setCanceledOnTouchOutside(!disableClickOutside)
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreatedListeners.forEach { it.invoke(view, savedInstanceState) }
    }

    override val disposeBag: MutableList<() -> Unit>
        get() = mutableListOf()

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    override fun setLoading(isLoading: Boolean, vararg labels: String) {
        (this.activity as? UIController)?.setLoading(isLoading, *labels)
    }

    override fun showToast(message: String, toastType: Alert.ToastType) {
        (this.activity as? UIController)?.showToast(message, toastType)
    }

    override fun showPopup(
        title: String?,
        message: String?,
        subMessage: CustomSpannable?,
        buttonLabel: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?
    ) {
        (this.activity as? UIController)?.showPopup(
            title,
            message,
            subMessage,
            buttonLabel,
            heroImage,
            onDismiss
        )
    }

    override fun showPopupWithMultiButton(
        title: String?,
        message: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
        multiButtonType: Alert.MultiButtonType
    ) {
        (this.activity as? UIController)?.showPopupWithMultiButton(
            title,
            message,
            heroImage,
            onDismiss,
            negativeButtonLabel,
            negativeButtonListener,
            positiveButtonLabel,
            positiveButtonListener
        )
    }

    override fun showNativePopup(
        title: String?,
        message: String?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
    ) {
        (this.activity as? UIController)?.showNativePopup(
            title,
            message,
            positiveButtonLabel,
            positiveButtonListener,
            negativeButtonLabel,
            negativeButtonListener
        )
    }

    override fun launchDeepLink(uri: String, internalOnly: Boolean, onNotSupported: (() -> Unit)?) {
        (this.activity as? UIController)?.launchDeepLink(uri, internalOnly, onNotSupported)
    }

    override fun showNoConnectionError(onConnected: () -> Unit) {
        (this.activity as? UIController)?.showNoConnectionError(onConnected)
    }

    override fun showPopupContactToMedicalCouncil(title: String) {
        (this.activity as? UIController)?.showPopupContactToMedicalCouncil(title)
    }

    override fun restartSession() {
        (this.activity as? UIController)?.restartSession()
    }

    override fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)?,
        onGranted: () -> Unit,
    ) {
        (this.activity as? UIController)?.requestPermissions(
            *permissions,
            onDenied = onDenied,
            onGranted = onGranted
        )
    }

    override fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit,
    ) {
        (this.activity as? UIController)?.requestPermissionsWithRationale(
            *permissions,
            onResult = onResult
        )
    }

    override fun gotoPage(navDirections: NavDirections) {
        (this.activity as? UIController)?.gotoPage(navDirections)
    }

    override fun goBack(@IdRes destinationId: Int?) {
        (this.activity as? UIController)?.goBack(destinationId)
    }

    override fun onCallPhoneNumber(phoneNo: String) {
        (this.activity as? UIController)?.onCallPhoneNumber(phoneNo)
    }

    override fun onCallEmail(email: String) {
        (this.activity as? UIController)?.onCallEmail(email)
    }

    override fun onCallWebBrowser(link: String) {
        (this.activity as? UIController)?.onCallWebBrowser(link)
    }

    override fun onDeeplinkPreRoute(continuation: Continuation<Unit>) {
        continuation.resume(Unit)
    }
}
