package com.krungsri.appfoundation.text

import android.text.InputFilter
import android.text.Spanned

class SpecialCharactersInputFilter : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        return if (CommonRegex.SPECIAL_CHARACTERS.contains("" + source)) "" else null
    }
}
