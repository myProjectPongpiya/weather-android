package com.krungsri.appfoundation.text

@SuppressWarnings("MagicNumber") // Use hardcoded numbers for indexes of formatting string
enum class AutoFormatType(
    val formatChar: String,
    val maxLength: Int?,
    val block: (CharSequence, Int, String) -> CharSequence
) {

    NO_FORMAT(FormatUtils.FORMAT_BLANK, null, { input, _, _ ->
        input
    }),
    PHONE_FORMAT(FormatUtils.FORMAT_DASH, null, { input, _, _ ->
        input
    }),
    MOBILE_NO(FormatUtils.FORMAT_DASH, 12, { input, index, formatChar ->
        when (index) {
            3, 6 -> "$formatChar$input"
            else -> input
        }
    }),
    PHONE_NO(FormatUtils.FORMAT_DASH, 11, { input, index, formatChar ->
        when (index) {
            3, 6 -> "$formatChar$input"
            else -> input
        }
    }),
    PHONE_NO_IN_BKK(FormatUtils.FORMAT_DASH, 11, { input, index, formatChar ->
        when (index) {
            2, 5 -> "$formatChar$input"
            else -> input
        }
    });

    fun getAppendedText(input: CharSequence, index: Int) = block(input, index, formatChar)
}
