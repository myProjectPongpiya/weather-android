package com.krungsri.appfoundation.text

import android.text.Editable
import android.text.TextWatcher

interface DrBaseTextWatcher : TextWatcher {
    override fun afterTextChanged(editable: Editable) = Unit
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = Unit
}
