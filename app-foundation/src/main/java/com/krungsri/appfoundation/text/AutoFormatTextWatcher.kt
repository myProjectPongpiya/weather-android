package com.krungsri.appfoundation.text

import android.text.Editable

class AutoFormatTextWatcher(
    private val formatType: AutoFormatType,
    private var formattedString: String = ""
) : DrBaseTextWatcher {

    override fun afterTextChanged(editable: Editable) {
        if (editable.toString() != formattedString) {
            formattedString = FormatUtils.formatInputByType(editable.toString(), formatType)
            editable.replace(0, editable.length, formattedString, 0, formattedString.length)
        }
    }
}
