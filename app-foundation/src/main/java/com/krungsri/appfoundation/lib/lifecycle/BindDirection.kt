package com.krungsri.appfoundation.lib.lifecycle

enum class BindDirection {
    ONE_WAY,
    TWO_WAY
}
