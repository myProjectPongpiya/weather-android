package com.krungsri.appfoundation.lib.lifecycle

import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.krungsri.appfoundation.extensions.setImage
import com.krungsri.appfoundation.graphics.DrImage

/**
 * Binds the image resource of this [ImageView] to [liveData].
 */
fun ImageView.bindImageResource(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImageResource(it ?: 0)
    }
}

/**
 * Binds the [BNImage] of [liveData].
 */
fun ImageView.bindImage(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<DrImage>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImage(it)
    }
}
