package com.krungsri.appfoundation.lib.eventbus

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.launch
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

class EventBus(
    override val coroutineContext: CoroutineContext = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
) : CoroutineScope {

    val bus: BroadcastChannel<AppEvent> = BroadcastChannel(1)

    /**
     * Publish an [event] to the event bus.
     */
    fun publish(event: AppEvent) {
        launch { bus.send(event) }
    }

    /**
     * Listen for events of a specific [type].
     */
    inline fun <reified T> listen(type: Class<T>): Flow<T> {
        return bus.asFlow().filterIsInstance()
    }
}
