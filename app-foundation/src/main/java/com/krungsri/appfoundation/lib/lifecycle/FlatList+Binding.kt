package com.krungsri.appfoundation.lib.lifecycle

import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.krungsri.appfoundation.flatlist.DrFlatListItem
import com.krungsri.appfoundation.list.DrFlatList

/**
 * Bind the text of this [TextView] to this [liveData].
 */
fun DrFlatList.bindItems(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<List<DrFlatListItem>>
) = apply {
    liveData.observe(lifecycleOwner) { this.setItems(it) }
}
