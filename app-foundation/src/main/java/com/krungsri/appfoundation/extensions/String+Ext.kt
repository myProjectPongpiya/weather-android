package com.krungsri.appfoundation.extensions

import com.krungsri.appfoundation.text.FormatType

fun String.formatType(type: FormatType, isMask: Boolean = false) = type.format(this, isMask)

fun String?.takeIfNotBlank(): String? {
    return this.takeIf { it?.isNotBlank() == true }
}

fun String.trimCharacters(vararg chars: String): String {
    return chars.fold(this) { acc, s -> acc.trimCharacter(s) }
}

fun String.trimCharacter(char: String): String {
    return this.replace(char, "")
}

fun String?.convertEmptyToNull() = when {
    this.isNullOrEmpty() -> null
    this.isNotEmpty() -> this
    else -> null
}
