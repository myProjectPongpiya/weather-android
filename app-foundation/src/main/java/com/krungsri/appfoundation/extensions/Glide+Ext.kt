package com.krungsri.appfoundation.extensions

import android.graphics.drawable.Drawable
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.krungsri.appfoundation.graphics.DrImage

/**
 * Loads a [DrImage] using Glide.
 */
fun RequestManager.loadImage(
    image: DrImage
): RequestBuilder<Drawable> {
    // Apply image source
    val builder = when (image) {
        is DrImage.Url -> this.load(image.url)
        is DrImage.ResId -> this.load(image.resId)
        is DrImage.Bitmap -> this.load(image.bitmap)
    }

    // Apply fallbacks
    when (image) {
        is DrImage.Url -> {
            image.error?.let { builder.error(it) }
            image.placeholder?.let { builder.placeholder(it) }
        }
        else -> {
            // do nothing
        }
    }

    // Apply transformations
    val glideTransformations = image.transformations
        .map { it.toGlideTransformation() }
        .takeIf { it.isNotEmpty() }
    glideTransformations?.let { builder.transform(*it.toTypedArray()) }

    // Apply resizing
    builder.override(image.resizeWidth, image.resizeHeight)

    return builder
}
