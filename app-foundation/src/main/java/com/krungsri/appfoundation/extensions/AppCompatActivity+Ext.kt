package com.krungsri.appfoundation.extensions

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity

/**
 * Launches a deep link [uri]. You may optionally restrict the deep link to
 * the current application via the [internalOnly] flag.
 *
 * If there are no applications that can support the deep link, [onNotSupported]
 * will be invoked.
 */
fun AppCompatActivity.launchDeeplink(
    uri: Uri,
    internalOnly: Boolean = false,
    onNotSupported: (() -> Unit)? = null
) {
    val intent = Intent(Intent.ACTION_VIEW, uri)

    if (internalOnly) {
        intent.setPackage(packageName)
    }

    // Verify that there's an app that can support this deep link.
    if (packageManager.queryIntentActivities(intent, 0).size < 1) {
        onNotSupported?.invoke()
        return
    }

    startActivity(intent)
}
