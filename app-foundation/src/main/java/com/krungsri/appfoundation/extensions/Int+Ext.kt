package com.krungsri.appfoundation.extensions

import android.content.res.Resources
import kotlin.math.roundToInt

fun Int?.toIntWithDefault(default: Int): Int {
    return this ?: default
}

fun Int?.isGreaterThan(other: Int?): Boolean {
    return this != null && other != null && this > other
}

fun Int?.isGreaterThanOrEqual(other: Int?): Boolean {
    return this != null && other != null && this >= other
}

fun Int?.isLessThan(other: Int?): Boolean {
    return this != null && other != null && this < other
}

fun Int?.isLessThanOrEqual(other: Int?): Boolean {
    return this != null && other != null && this <= other
}

fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).roundToInt()

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).roundToInt()
