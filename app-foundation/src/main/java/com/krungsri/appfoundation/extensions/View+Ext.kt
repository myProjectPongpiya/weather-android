package com.krungsri.appfoundation.extensions

import android.view.View

/**
 * Change this view's visibility to [View.VISIBLE].
 */
fun View.visible() {
    this.visibility = View.VISIBLE
}

/**
 * Change this view's visibility to [View.GONE].
 */
fun View.gone() {
    this.visibility = View.GONE
}

/**
 * Change this view's visibility to [View.GONE] if [predicate] returns `true`,
 * otherwise defaults to [default].
 */
fun View.goneIf(default: Int = View.VISIBLE, predicate: () -> Boolean) {
    if (predicate.invoke()) {
        this.visibility = View.GONE
    } else {
        this.visibility = default
    }
}

/**
 * Change this view's visibility to [View.INVISIBLE].
 */
fun View.invisible() {
    this.visibility = View.INVISIBLE
}

/**
 * Change this view's visibility to [View.INVISIBLE] if [predicate] returns
 * `true`, otherwise defaults to [default].
 */
fun View.invisibleIf(default: Int = View.VISIBLE, predicate: () -> Boolean) {
    if (predicate.invoke()) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = default
    }
}
