package com.krungsri.appfoundation.extensions

import android.net.Uri

/**
 * Returns the path of this [Uri] including its query parameters.
 */
fun Uri.toPathWithQueryParameters(s: String): String? {
    val prefix = buildString {
        scheme?.let { append("$it://") }
        host?.let { append(it) }
    }
    return toString().removePrefix(prefix).removePrefix("?").split("&")
        .associate {
            val (left, right) = it.split("=")
            left to right
        }[s]
}
