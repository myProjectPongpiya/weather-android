package com.krungsri.appfoundation.extensions

/**
 * Returns the resulting value of [func], or `null` if an exception occurs.
 */
inline fun <T> tryOrNull(func: () -> T) = try {
    func.invoke()
} catch (_: Throwable) {
    null
}
