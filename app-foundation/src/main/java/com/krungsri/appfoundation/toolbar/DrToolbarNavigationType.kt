package com.krungsri.appfoundation.toolbar

enum class DrToolbarNavigationType {
    BACK,
    CLOSE,
    BOTH
}
