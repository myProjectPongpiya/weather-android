package com.krungsri.appfoundation.toolbar

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import com.krungsri.appfoundation.R
import com.krungsri.appfoundation.ui.SingleClickListener

/**
 * [DrToolbarMenu] is a view group that renders a set of [DrToolbarMenuItem]
 * using the [setMenuItems] method.
 */
internal class DrToolbarMenu @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    private var color: Int? = null
    private var onClick: ((String) -> Unit)? = null

    init {
        this.orientation = HORIZONTAL
        this.gravity = Gravity.CENTER_VERTICAL
    }

    /**
     * Register a callback to be invoked when a menu item is clicked. The callback
     * will be invoked with the `id` of the [DrToolbarMenuItem].
     */
    fun setMenuItemClickListener(onClick: (id: String) -> Unit) {
        this.onClick = onClick
    }

    /**
     * Renders [items] as the menu.
     */
    fun setMenuItems(items: List<DrToolbarMenuItem>, side: Side = Side.NONE) {
        this.removeAllViews()
        items.forEachIndexed { index, item ->
            val isLeftEdge = side == Side.LEFT && index == 0
            val isRightEdge = side == Side.RIGHT && index == items.size - 1
            val container = createContainer(item, isLeftEdge, isRightEdge)

            val view = when (item) {
                is DrToolbarMenuItem.IconButton -> createImageView(item)
                is DrToolbarMenuItem.TextButton -> createTextView(item)
            }

            view.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO

            container.addView(view)
            this.addView(container)
        }
    }

    private fun onMenuItemClick(id: String) {
        onClick?.invoke(id)
    }

    private fun createContainer(
        item: DrToolbarMenuItem,
        isLeftEdge: Boolean,
        isRightEdge: Boolean
    ): FrameLayout {
        val layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.MATCH_PARENT
        )

        val leftPadding = if (isLeftEdge) 21f.toDp().toInt() else 4f.toDp().toInt()
        val rightPadding = if (isRightEdge) 21f.toDp().toInt() else 4f.toDp().toInt()

        val clickListener = SingleClickListener(
            listener = OnClickListener { onMenuItemClick(item.id) },
            throttleInterval = 1000,
            globalThrottle = false
        )

        return FrameLayout(context).apply {
            setLayoutParams(layoutParams)
            setPadding(leftPadding, 0, rightPadding, 0)
            setOnClickListener(clickListener)
            tag = id
            isClickable = true
        }
    }

    private fun createTextView(item: DrToolbarMenuItem.TextButton): AppCompatTextView {
        val layoutParams = FrameLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        )

        return AppCompatTextView(context).apply {
            setLayoutParams(layoutParams)
            TextViewCompat.setTextAppearance(this, R.style.TextAppearance_ToolbarButton)
            id = R.id.toolbarLabel
            addCircleRipple()
            text = item.label
            contentDescription = item.label
        }
    }

    private fun createImageView(item: DrToolbarMenuItem.IconButton): ImageView {
        val imageResId = item.iconId
        val layoutParams = FrameLayout.LayoutParams(
            context.resources.getDimensionPixelSize(R.dimen.spacing_5),
            context.resources.getDimensionPixelSize(R.dimen.spacing_5),
            Gravity.CENTER
        )

        return ImageView(context).apply {
            setLayoutParams(layoutParams)
            setImageResource(imageResId)
            addCircleRipple()
            id = R.id.toolbarIcon
            contentDescription = item.contentDescription
            if (color != null) {
                imageTintList = ColorStateList.valueOf(color!!)
            }
        }
    }

    private fun View.addCircleRipple() = with(TypedValue()) {
        context.theme.resolveAttribute(android.R.attr.actionBarItemBackground, this, true)
        setBackgroundResource(resourceId)
    }

    private fun Float.toDp(): Float = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        this@DrToolbarMenu.resources.displayMetrics
    )

    fun changeState(isEnable: Boolean) {
        (this.getChildAt(0) as? FrameLayout)?.let {
            it.isClickable = isEnable
            (it.getChildAt(0) as? TextView)?.let {
                if (isEnable) {
                    TextViewCompat.setTextAppearance(it, R.style.TextAppearance_ToolbarButton)
                } else {
                    TextViewCompat.setTextAppearance(it, R.style.TextAppearance_ToolbarButton_Disabled)
                }
            }
            (it.getChildAt(0) as? ImageView)?.let {
                if (isEnable) {
                    ImageView.ALPHA.set(it, 1.0f)
                } else {
                    ImageView.ALPHA.set(it, 0.25f)
                }
            }
        }
    }

    fun setTintColor(color: Int) {
        this.color = color
    }

    /**
     * Represents which side this menu appears in.
     */
    enum class Side { LEFT, RIGHT, NONE }
}
