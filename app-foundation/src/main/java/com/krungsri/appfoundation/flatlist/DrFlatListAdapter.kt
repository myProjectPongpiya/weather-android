package com.krungsri.appfoundation.flatlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.appfoundation.R

internal class DrFlatListAdapter : RecyclerView.Adapter<DrFlatListViewHolder>() {

    private val items = mutableListOf<DrFlatListItem>()
    private var onClickListener: ((item: DrFlatListItem.Item) -> Unit)? = null
    private var titlePaddingButton: Float? = null
    private var horizontalPadding: Float? = null
    private var itemPaddingBottom: Float? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrFlatListViewHolder {
        val layoutRes = when (viewType) {
            HEADER_TYPE -> R.layout.item_common_list_header
            ITEM_TYPE -> R.layout.item_flat_list
            LOAD_TYPE -> R.layout.item_progress_bar
            else -> throw IllegalStateException("Unknown viewType: $viewType")
        }

        val view = LayoutInflater
            .from(parent.context)
            .inflate(layoutRes, parent, false)

        return DrFlatListViewHolder(view, this::onClickViewHolder)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DrFlatListViewHolder, position: Int) {
        holder.setData(items[position])
        titlePaddingButton?.let { holder.setTitlePadding(it) }
        horizontalPadding?.let { holder.setHorizontalPadding(it) }
        itemPaddingBottom?.let { holder.setItemPaddingBottom(it) }
    }

    private fun onClickViewHolder(position: Int) {
        if (position == -1) {
            return
        }
        (items[position] as? DrFlatListItem.Item)?.let {
            onClickListener?.invoke(it)
        }
    }

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is DrFlatListItem.Header -> HEADER_TYPE
        is DrFlatListItem.Item -> ITEM_TYPE
        is DrFlatListItem.Load -> LOAD_TYPE
    }

    /**
     * Sets [items] in this [RecyclerView].
     */
    fun setItems(items: List<DrFlatListItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    /**
     * Load more [items] in this [RecyclerView].
     */
    fun loadMoreItems(items: List<DrFlatListItem>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    /**
     * Clear [items] in this [RecyclerView].
     */
    fun clearItems() {
        this.items.clear()
        notifyDataSetChanged()
    }

    /**
     * Sets a [listener] to be invoked when an item is clicked.
     */
    fun setOnClickListener(listener: ((DrFlatListItem.Item) -> Unit)?) {
        this.onClickListener = listener
    }

    /**
     * Sets the title [padding] of the items.
     */
    fun setTitlePaddingButton(padding: Float?) {
        titlePaddingButton = padding
    }

    /**
     * Sets the horizontal [padding] of the items.
     */
    fun setHorizontalPadding(padding: Float) {
        horizontalPadding = padding
    }

    /**
     * Sets the Item Padding Bottom [padding] of the items.
     */
    fun setItemPaddingBottom(padding: Float) {
        itemPaddingBottom = padding
    }

    fun showProgressBar() {
        val lastItemPosition = items.size - 1
        items.add(DrFlatListItem.Load())
        notifyItemInserted(lastItemPosition)
    }

    fun hideProgressBar() {
        val lastItemPosition = items.size - 1
        if (lastItemPosition != INVALID_POSITION &&
            items[lastItemPosition] is DrFlatListItem.Load
        ) {
            items.removeAt(lastItemPosition)
            notifyItemRemoved(lastItemPosition)
        }
    }

    fun getItems() = items

    companion object {
        const val HEADER_TYPE = 1
        const val ITEM_TYPE = 2
        const val LOAD_TYPE = 3
        const val INVALID_POSITION = -1
    }
}
