package com.krungsri.appfoundation.permissions

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.markodevcic.peko.Peko
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

/**
 * [PermissionUtils] is a wrapper around [Peko] to simplify the process
 * of requesting for permissions.
 */
object PermissionUtils : CoroutineScope {
    private var onRequesting = false

    private val job = Job()
    private var permissionJob: Job? = null
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    /**
     * Checks for required [permissions].
     *
     * @param onDenied The callback to be invoked when any [permissions] is denied.
     * @param onRationaleRequired The callback is needed to allow permission request again.
     * @param onGranted The callback to be invoked when all permissions are granted.
     */
    fun checkPermissions(
        activity: Activity,
        permissions: List<String>,
        onDenied: (() -> Unit)? = null,
        onRationaleRequired: ((PermissionResult.RationaleRequired) -> Unit)? = null,
        onGranted: () -> Unit
    ) {
        if (!onRequesting) {
            val rationalRequired = PermissionResult.RationaleRequired(
                activity,
                permissions,
                onDenied,
                onGranted
            )
            if (shouldShowRequestPermissionRationale(activity, permissions) && onRationaleRequired != null) {
                onRationaleRequired.invoke(rationalRequired)
            } else {
                onAskPermissions(rationalRequired)
            }
        }
    }

    /**
     * Checks for required [permission].
     *
     * @param onDenied The callback to be invoked when the [permission] is denied.
     * @param onRationaleRequired The callback is needed to allow permission request again.
     * @param onGranted The callback to be invoked when all permissions are granted.
     */
    fun checkPermission(
        activity: Activity,
        permission: String,
        onDenied: (() -> Unit)? = null,
        onRationaleRequired: ((PermissionResult.RationaleRequired) -> Unit)? = null,
        onGranted: () -> Unit
    ) {
        checkPermissions(
            activity = activity,
            permissions = listOf(permission),
            onDenied = onDenied,
            onRationaleRequired = onRationaleRequired,
            onGranted = onGranted
        )
    }

    /**
     * Request required [permission].
     */
    fun onAskPermissions(rationaleRequired: PermissionResult.RationaleRequired) {
        if (!onRequesting) {
            permissionJob?.cancel()
            permissionJob = launch {
                onRequesting = true
                Peko.requestPermissionsAsync(
                    rationaleRequired.activity,
                    *rationaleRequired.permissions.toTypedArray()
                ).let {
                    onRequesting = false
                    if (it is com.markodevcic.peko.PermissionResult.Granted) {
                        rationaleRequired.onGranted.invoke()
                    } else {
                        rationaleRequired.onDenied?.invoke()
                    }
                }
            }
            permissionJob?.start()
        }
    }

    /**
     * Check required [permission] whether should show rationale dialog or not.
     */
    private fun shouldShowRequestPermissionRationale(
        activity: Activity,
        permissions: List<String>
    ): Boolean {
        return permissions.any { permission ->
            ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED &&
                ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
        }
    }
}
