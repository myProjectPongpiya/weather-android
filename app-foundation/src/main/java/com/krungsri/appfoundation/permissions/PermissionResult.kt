package com.krungsri.appfoundation.permissions

import android.app.Activity

/**
 * Represents a result for a permission request.
 */
sealed class PermissionResult {
    object Granted : PermissionResult()
    object Denied : PermissionResult()
    data class RationaleRequired(
        val activity: Activity,
        val permissions: List<String>,
        val onDenied: (() -> Unit)? = null,
        val onGranted: () -> Unit
    ) : PermissionResult()
}
