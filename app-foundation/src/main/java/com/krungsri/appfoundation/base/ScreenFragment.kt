package com.krungsri.appfoundation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.ViewCompat
import com.krungsri.appfoundation.R
import com.krungsri.appfoundation.toolbar.DrToolbar
import com.krungsri.appfoundation.toolbar.DrToolbarNavigationType

/**
 * [ScreenFragment] is a base class for fragments that can be presented as
 * a standalone screen. It contains various utility classes and sane defaults
 * that are relevant for screens.
 */
abstract class ScreenFragment(
    @LayoutRes private val contentLayoutId: Int? = null
) : BaseFragment() {

    /**
     * By default, the fragment looks for a view with [R.id.toolbar] to designate
     * as the screen's Toolbar. Override [toolbarId] if the toolbar uses another id.
     */
    protected open var toolbarId: Int = R.id.toolbar

    /**
     * An instance of the screen's [DrToolbar].
     * @see toolbarId
     */
    protected var toolbar: DrToolbar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (contentLayoutId != null) {
            inflater.inflate(contentLayoutId, container, false)
        } else {
            null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = view.findViewById(toolbarId)
        toolbar?.let { setupToolbar(it) }

        // Window insets do not getDeviceId propagated to children unless this is called.
        // This is required for DrToolbar to configure its status bar height.
        ViewCompat.requestApplyInsets(view)
    }

    private fun setupToolbar(toolbar: DrToolbar) {
        setToolbarNavigationType(DrToolbarNavigationType.BACK)
    }

    /**
     * Sets the toolbar navigation [type], invoking [onClick] upon clicking. By
     * default, the callback simulates a back button event.
     */
    protected fun setToolbarNavigationType(
        type: DrToolbarNavigationType,
        onClick: (DrToolbarNavigationType) -> Unit = { navigateBack() }
    ) {
        when (type) {
            DrToolbarNavigationType.BACK -> {
                toolbar?.setRightMenu { }
                toolbar?.setLeftButton(R.drawable.ic_arrow_back, "Navigate Up") {
                    onClick(DrToolbarNavigationType.BACK)
                }
            }
            DrToolbarNavigationType.CLOSE -> {
                toolbar?.setLeftMenu { }
                toolbar?.setRightButton(R.drawable.ic_close_screen, "Navigate Out") {
                    onClick(DrToolbarNavigationType.CLOSE)
                }
            }
            DrToolbarNavigationType.BOTH -> {
                toolbar?.setLeftButton(R.drawable.ic_arrow_back, "Navigate Up") {
                    onClick(DrToolbarNavigationType.BACK)
                }
                toolbar?.setRightButton(R.drawable.ic_close_screen, "Navigate Out") {
                    onClick(DrToolbarNavigationType.CLOSE)
                }
            }
        }
    }

    /**
     * Invoked when a navigation to return to this screen is performed.
     */
    open fun onReturn() {
    }

    /**
     * Invokes an user's intent to navigate back.
     *
     * The difference between using this method over calling [Backstack.goBack] is
     * that this ensures [onBackPressed] is called.
     */
    open fun navigateBack() {
        activity?.onBackPressed()
    }

    /**
     * Called when the [FragmentHostActivity] has detected the user's press of
     * the back key.
     *
     * Should return `true` if the back button has been handled. Otherwise
     * returning `false` would continue to defer the back button handling to the
     * default behavior.
     *
     * WARNING: This function should not be called independently to manually
     *          invoke the back button behavior. For that, you should invoke
     *          the `onBackPressed` function of the host activity instead.
     */
    open suspend fun onBackPressed(): Boolean {
        return false
    }
}
