package com.krungsri.appfoundation.menu

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.view.MenuItemCompat
import androidx.core.view.forEach
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.krungsri.appfoundation.R
import kotlinx.android.synthetic.main.layout_bottom_navigation.view.tabWidget
import kotlin.math.roundToInt

class BottomTabWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0,
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    enum class BottomTabNav(val navId: Int) {
        MAIN_TAB(BottomTabWidget.MAIN_TAB),
        PROFILE_TAB(BottomTabWidget.PROFILE_TAB),
    }

    private var navBottomTabItemListener: NavBottomTabItemListener? = null
    private var onMenuPrepareListener: ((Unit) -> Unit?)? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_bottom_navigation, this, true)
    }

    fun setNavBottomTabItemListener(listener: NavBottomTabItemListener) {
        navBottomTabItemListener = listener
    }

    @SuppressLint("RestrictedApi")
    fun initMenu(activity: Activity) {
        val menu = MenuBuilder(activity)
        activity.menuInflater.inflate(R.menu.menu_bottom, menu)
        setMenuItems(menu)
        setMarginIcon(menu)
        initMenuItemListener()
    }

    private fun initMenuItemListener() {
        tabWidget.setOnNavigationItemSelectedListener { item ->

            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.navigation_main -> checkNavBarAction(
                    BottomTabNav.MAIN_TAB.navId
                )
                R.id.navigation_profile -> checkNavBarAction(
                    BottomTabNav.PROFILE_TAB.navId
                )
                else -> true
            }
        }
    }

    private fun activeTabPerformClick(activeTab: Int) {
        when (activeTab) {
            BottomTabNav.MAIN_TAB.navId ->
                tabWidget.menu.performIdentifierAction(R.id.navigation_main, 0)
            BottomTabNav.PROFILE_TAB.navId ->
                tabWidget.menu.performIdentifierAction(R.id.navigation_profile, 0)
        }
    }

    private fun checkNavBarAction(tabId: Int): Boolean {
        navBottomTabItemListener?.navServiceItemChange(tabId)
        return true
    }

    private fun setMenuItems(menuBuilder: Menu) {
        tabWidget.menu.clear()
        tabWidget.itemIconTintList = null
        menuBuilder.forEach { menu ->
            val label = menu.title.toString()
            val contentDescriptionText = menu.titleCondensed.toString()

            tabWidget.menu.add(Menu.NONE, menu.itemId, Menu.NONE, label).apply {
                icon = menu.icon
                MenuItemCompat.setContentDescription(
                    this,
                    contentDescriptionText
                )
            }
        }
        onMenuPrepareListener?.invoke(Unit)
    }

    private fun setMarginIcon(menuBuilder: Menu) {
        menuBuilder.forEach { menuItem ->
            val menu: BottomNavigationItemView = findViewById(menuItem.itemId)
//            val icon: ImageView = menu.findViewById(com.google.android.material.R.id.icon)
//            setMargins(
//                icon,
//                MARGIN_OTHER_ICON_NAV_BAR,
//                dpToPx(MARGIN_TOP_ICON_NAV_BAR),
//                MARGIN_OTHER_ICON_NAV_BAR,
//                MARGIN_OTHER_ICON_NAV_BAR
//            )
        }
    }

    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is MarginLayoutParams) {
            val p = view.layoutParams as MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    fun dpToPx(dp: Float): Int {
        return (dp * resources.displayMetrics.density.toInt()).roundToInt()
    }

    fun setBottomNavActiveTab(index: Int?) {
        when (index) {
            MAIN_TAB -> setActiveTab(BottomTabNav.MAIN_TAB.navId)
            PROFILE_TAB -> setActiveTab(BottomTabNav.PROFILE_TAB.navId)
            else -> return
        }
    }

    /**
     * Change active tab. For [MAIN_TAB] and[PROFILE_TAB]
     **/
    fun setActiveTab(activeTab: Int) {
        tabWidget.post {
            tabWidget.selectedItemId = when (activeTab) {
                BottomTabNav.MAIN_TAB.navId -> R.id.navigation_main
                BottomTabNav.PROFILE_TAB.navId -> R.id.navigation_profile
                else -> R.id.navigation_main
            }
        }
    }

    interface NavBottomTabItemListener {
        fun navServiceItemChange(activeTab: Int)
    }

    companion object {
        const val MAIN_TAB = 0
        const val PROFILE_TAB = 1

        const val MARGIN_TOP_ICON_NAV_BAR = 11f
        const val MARGIN_OTHER_ICON_NAV_BAR = 0
    }
}

inline fun BottomTabWidget.setNavBottomTabItemListener(crossinline block: (Int) -> Unit) {
    this.setNavBottomTabItemListener(object : BottomTabWidget.NavBottomTabItemListener {
        override fun navServiceItemChange(activeTab: Int) = block(activeTab)
    })
}
