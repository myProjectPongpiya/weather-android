package com.krungsri.appfoundation.http

import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class ApiHeader(
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("errorDisplay")
    val errorDisplay: ErrorDisplay? = null,
)