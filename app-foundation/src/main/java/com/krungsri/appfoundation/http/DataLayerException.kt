package com.krungsri.appfoundation.http

import java.io.IOException

class DataLayerException(override val cause: Throwable?) :
    IOException("An error occurred in the HTTP layer", cause)
