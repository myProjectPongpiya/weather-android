package com.krungsri.appfoundation.http.interceptor

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.krungsri.appfoundation.http.ApiException
import com.krungsri.appfoundation.http.ApiHeader
import com.krungsri.appfoundation.http.ApiHeaderResp
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * The [CommonExceptionInterceptor] intercepts all incoming responses from the API
 * and throws an [ApiException] if necessary.
 *
 * [ApiException] is thrown when the HTTP status code is not any of the [200...300]
 * series. If an [ApiHeader] body is present, it is included in the exception, or
 * `null` otherwise.
 *
 * The [ApiException] also includes the fatal [Response] that triggered the error
 * for further troubleshooting.
 */
class CommonExceptionInterceptor(private val gson: Gson) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.isSuccessful) {
            return response // Proceed as usual
        }

        val apiHeader = parseForError(response.peekBody(Long.MAX_VALUE))
        throw ApiException(apiHeader?.headerResp, response)
    }

    /**
     * Maps the response body to an [ApiHeader] object. Returns `null` if it is
     * unable to perform the mapping.
     */
    private fun parseForError(body: ResponseBody?): ApiHeaderResp? {
        if (body == null) return null
        return try {
            this.gson.fromJson(body.string(), ApiHeaderResp::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
    }
}
