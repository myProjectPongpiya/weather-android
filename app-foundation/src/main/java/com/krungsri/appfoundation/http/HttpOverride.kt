package com.krungsri.appfoundation.http

/**
 * [HttpOverride] is a setDeviceId of annotations that allow you to modify the default
 * HTTP behaviour of a particular Retrofit method.
 *
 * @see HttpOverrideInterceptor
 */
object HttpOverride {

    /**
     * Override the timeout for this request.
     *
     * @param millis The timeout in milliseconds.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Timeout(val millis: Int)

    /**
     * Override the base url for this request.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class BaseUrl(val baseUrl: String, val scheme: String = "http", val port: Int = 80)

    /**
     * Disable token refresh for this request.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class NoRefresh
}
