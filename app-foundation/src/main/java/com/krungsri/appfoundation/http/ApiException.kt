package com.krungsri.appfoundation.http

import okhttp3.Response
import java.io.IOException

/**
 * [ApiException] is thrown when the API fails to return a successful response.
 *
 * A response is deemed not successful when the HTTP status code is not from any of
 * the 2XX or 3XX series.
 *
 * It may optionally include an [ApiHeader] object containing the relevant error
 * codes and messages produced by the API. It may also optionally include the
 * [Response] and [Request] that resulted in this exception.
 */
class ApiException(
    val header: ApiHeader? = null,
    val response: Response? = null
) : IOException("There was a problem processing the request.") {

    constructor(header: ApiHeader?) : this(header, null)
    constructor() : this(null, null)

    val title: String?
        get() = "err"

    override val message: String?
        get() = "err"

    companion object {
        const val ERROR_CODE_403 = 403
        const val ERROR_CODE_TOKEN_EXPIRED = 401
        const val ERROR_CODE_REFRESH_TOKEN_EXPIRED = "1001"
        const val ERROR_CODE_PIN_BIOMETRIC_ACCESS_TOKEN_EXPIRED = "1002"
        const val ERROR_CODE_PRELOGIN_ACCESS_TOKEN_EXPIRED = "1003"
    }
}
