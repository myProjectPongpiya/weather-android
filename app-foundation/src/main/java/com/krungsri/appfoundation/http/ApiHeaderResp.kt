package com.krungsri.appfoundation.http

import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class ApiHeaderResp(
    @SerializedName("headerResp")
    val headerResp: ApiHeader? = null
)
