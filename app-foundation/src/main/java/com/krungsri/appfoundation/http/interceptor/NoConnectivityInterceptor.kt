package com.krungsri.appfoundation.http.interceptor

import android.net.ConnectivityManager
import com.krungsri.appfoundation.extensions.isConnected
import okhttp3.Interceptor
import okhttp3.Response

/**
 * An [Interceptor] that throws a [NoConnectivityException] if it is detected
 * that the [connectivityManager] has no active internet connection.
 */
class NoConnectivityInterceptor constructor(
    private val connectivityManager: ConnectivityManager
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (isConnected()) {
            return chain.proceed(chain.request())
        }
        throw NoConnectivityException()
    }

    private fun isConnected() = connectivityManager.isConnected()
}
