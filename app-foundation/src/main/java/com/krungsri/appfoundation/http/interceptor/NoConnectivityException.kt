package com.krungsri.appfoundation.http.interceptor

import java.io.IOException

class NoConnectivityException : IOException()
